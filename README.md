# AwesomeMessenger

(проект будет дорабатываться, автор сдаёт сессию и государственные экзамены по высшей математике)

## Супер-защищенный, децентрализованный, распределенный, отказоустойчивый, exactly-once мессенджер
### Описание

#### Сервер
Ассинхронный сервер, которые принимает запросы json формата.

Форматы

```json
{ "examples" : [
  {
    "action": "send-message",
    "content" : {
      "from" : "username_a",
      "to" : ["username_b"],
      "message" : "text"
    }
  },
  {
    "action": "registration",
    "content" : {
      "username" : "username",
      "password" : "password"
    }
  },
  {
    "action": "login",
    "content" : {
      "username" : "username",
      "password" : "password"
    }
  }
]}

```

## Установка

Пререквезиты: 
```
conan==1.59.0
```
### Linux
```bash
mkdir build && cd build
```
```bash
conan install .. -b missing
```
```bash
cmake ..
```
```bash
make
```