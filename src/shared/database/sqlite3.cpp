#include <shared/database/sqlite3.hpp>
#include <fmt/core.h>

namespace db::engine {

SQLiteDataBase::SQLiteDataBase(const std::string& db_path, bool& success, bool debug) 
  : db_(nullptr),
    debug_(debug) {
  if (sqlite3_open_v2(
        db_path.c_str(), 
        &db_, 
        SQLITE_OPEN_READWRITE | SQLITE_OPEN_CREATE, 
        nullptr
      ) != SQLITE_OK
  ) {
    success = false;
  } else {
    success = true;
  }
};

int SQLiteDataBase::Execute(
  const std::string& query, 
  DBCallback callback,
  void* data
) {
  char* error = nullptr;
  int result = sqlite3_exec(db_, query.c_str(), callback, data, &error);

  if (debug_ && error) {
    fmt::print(
      "Error was raised by SQLite3 during db execution: {}\n", error
    );
  }

  return result;
}

SQLiteDataBase::~SQLiteDataBase() {
  sqlite3_close(db_);
}


} // namespace db::engine