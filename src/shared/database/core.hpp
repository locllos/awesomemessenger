#pragma once

#include <string>
#include <sqlite3.h>


namespace db {

using DBCallback = int (*) (
            /* sqlite3 provided docs */
  void*,    /* Data provided in the 4th argument of sqlite3_exec() */
  int,      /* The number of columns in row */
  char**,   /* An array of strings representing fields in the row */
  char**    /* An array of strings representing column names */
);


class IDataBase { // Adapter
 public:

  virtual int Execute(
    const std::string& query, 
    DBCallback callback,
    void* data
  ) = 0;

  virtual ~IDataBase() = default;
};


} // namespace db