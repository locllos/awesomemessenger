#include <shared/database/core.hpp>
#include <shared/database/sqlite3.hpp>
#include <unordered_map>

class DBManager {
 public:
  bool Exists(const std::string& path);
  bool CreateTable(
    const std::string& table_name,
    const std::unordered_map<std::string, std::string>& columns
  );
  bool Open(const std::string& path);

};

// TODO