#pragma once

#include <shared/database/core.hpp>

namespace db::engine {

class SQLiteDataBase : public IDataBase {
 public:
  SQLiteDataBase(const std::string& db_path, bool& success, bool debug = false);

  int Execute(
    const std::string& query, 
    DBCallback callback,
    void* data
  ) override final;

  ~SQLiteDataBase() override;

 private:
  sqlite3* db_;
  bool debug_;
};

} // namespace db