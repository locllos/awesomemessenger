#pragma once

#include <cstdint>

namespace constants {

const uint32_t kDefaultBufferSize = 8 * 1024;


} // namespace constants 