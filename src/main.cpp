#include <fmt/core.h>
#include <boost/container/deque.hpp>
#include <gui/imgui.hpp>

#include <server/server.hpp>

int main() {
  boost::container::deque<std::string> strings;

  strings.push_back("story");

  fmt::print("It's gonna be an interesting {}\n", strings.back());  
  strings.pop_back();

  fmt::print("Beep bop. Starting `imgui` test...\n");
  ImGUITestRoutine();
  fmt::print("Beep bop. `imgui` test was ended successfully\n");

  fmt::print("Starting test an echo server on boost...\n");
  server::Server server("127.0.0.1", 8080);

  server.Run();

  return 0;
}
