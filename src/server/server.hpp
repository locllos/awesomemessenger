#include <shared/constants.hpp>
#include <boost/asio/io_context.hpp>
#include <shared/database/sqlite3.hpp>
#include <boost/asio.hpp>

namespace server {

using boost::asio::ip::tcp;
using boost::asio::ip::address;


class Server {
 private:
  struct Connection {
    explicit Connection(boost::asio::io_context& executor) : socket(executor) {
    }

    void Run();
    void HandleRead(size_t, const boost::system::error_code&);
    void HandleWrite(const boost::system::error_code&);
    
    void ProcessRequest();
    void Echo(size_t);

    tcp::socket socket;
    uint8_t data[constants::kDefaultBufferSize + 1];
  };

 public:

  Server(const std::string& address, int port);
  
  void Run();

  ~Server();

 private:
  // Server routine
  void Accept();
  void HandleAccept(Connection*, const boost::system::error_code&);

 private:
  boost::asio::io_context executor_;
  tcp::acceptor acceptor_;
  
  db::IDataBase* db_;
};


} // namespace server