#include <exception>
#include <server/server.hpp>
#include <shared/constants.hpp>

#include <boost/asio/placeholders.hpp>
#include <boost/bind/bind.hpp>
#include <boost/bind/placeholders.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>
#include <fmt/core.h>

#include <unistd.h>

namespace server {

void Server::Connection::Run() {
  socket.async_read_some(
    boost::asio::buffer(data, constants::kDefaultBufferSize + 1),
    [this] (const boost::system::error_code& error, size_t bytes_transferred) {
      this->HandleRead(bytes_transferred, error);
    }
  );
}

void Server::Connection::HandleRead(
  size_t bytes_transferred, 
  const boost::system::error_code& error
) {

  if (error) {
    fmt::print("Connection was stopped at {} with: {}\n", __func__, error.message());
    
    delete this;
    return;
  }

  // may be just check if the sent data matches to our format
  if (bytes_transferred >= constants::kDefaultBufferSize + 1) {
    fmt::print("Connection exceeded request size limit\n");
    // send an error that request size limit is exceeded

    Run();
    return;
  }
  data[bytes_transferred] = '\0';


  ProcessRequest();
  Echo(bytes_transferred);
}


void Server::Connection::HandleWrite(const boost::system::error_code& error) {
  if (error) {
    fmt::print("Connection was stopped at {} with: {}\n", __func__, error.message());
    
    delete this;
    return;
  }

  Run();
}

void Server::Connection::ProcessRequest() {
  fmt::print("Start request processing...");

  try {
    std::stringstream raw;
    raw << data;

    boost::property_tree::ptree request;
    boost::property_tree::read_json(raw, request);

    fmt::print("Got action: {}\n", request.get<std::string>("action"));
    if (request.get<std::string>("action") == "registration") {
      fmt::print(
        "Content: {}, {}\n", 
        request.get<std::string>("content.username"),
        request.get<std::string>("content.password")
      );
    } else if (request.get<std::string>("action") == "login") {
      fmt::print(
        "Content: {}, {}\n", 
        request.get<std::string>("content.username"),
        request.get<std::string>("content.password")
      );
    } else if (request.get<std::string>("action") == "send-message") {
      fmt::print(
        "Content: {}, {}\n", 
        request.get<std::string>("content.from"),
        request.get<std::string>("content.message")
      );
    }

  } catch (...) {
    fmt::print("Failed to parse JSON\n");
  }
}

void Server::Connection::Echo(size_t bytes_transferred) {
  socket.async_write_some(
    boost::asio::buffer(data, bytes_transferred),
    [this] (const boost::system::error_code& error, size_t /*bytes_transferred*/) {
      this->HandleWrite(error);
    }
  );
}

Server::Server(const std::string &address, int port) 
  : acceptor_(executor_, tcp::endpoint(
      address::from_string(address), port
    )) {
  bool success = true;
  db_ = new db::engine::SQLiteDataBase("data.db", success, true);

  if (!success) {
    delete db_;
    db_ = nullptr;

    fmt::print("Unsuccessful db opening\n");
    return;
  }

  db_->Execute(
    "CREATE TABLE IF NOT EXISTS \
     users (  \
       user_id   INTEGER       NOT NULL, \
       username  VARCHAR(20)   NOT NULL, \
       password  VARCHAR(255)  NOT NULL, \
       firstname VARCHAR(32), \
       surname   VARCHAR(32), \
       \
       CONSTRAINT PK_UserID PRIMARY KEY (user_id) \
     );",
    /*no callback*/nullptr, 
    /*no data*/nullptr);
}

void Server::Run() {
  Accept();

  executor_.run();
}


Server::~Server() {
  delete db_;
}

void Server::Accept() {
  auto connection = new Connection(executor_);

  acceptor_.async_accept(
    connection->socket, 
    [this, connection] (const boost::system::error_code& error) {
      this->HandleAccept(connection, error);
    }
  );
}

void Server::HandleAccept(
  Server::Connection* connection, 
  const boost::system::error_code& error
) {
  if (!error) {
    connection->Run();
  } else {
    fmt::print("Connection was stopped at {} with: {}\n", __func__, error.message());

    delete connection;
  }

  Accept(); 
}



} // namespace server