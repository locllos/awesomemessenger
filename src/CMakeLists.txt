add_executable(AwesomeMessenger main.cpp)
target_link_libraries(AwesomeMessenger ${CONAN_LIBS})

file(GLOB_RECURSE INCLUDE *.hpp)
file(GLOB_RECURSE SOURCE *.cpp)

target_include_directories(AwesomeMessenger
    PUBLIC
        .
    )

target_sources(AwesomeMessenger
    PUBLIC
        ${INCLUDE}
    PRIVATE
        ${SOURCE}
)